#!/usr/bin/python

import pandas as pd
import scipy as sp
import matplotlib as mp
import matplotlib.pyplot as plt

# Some Display Options
pd.options.display.max_rows = 100
pd.options.display.max_columns = 100

deaths_file = r'https://github.com/CSSEGISandData/COVID-19/raw/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_deaths_global.csv'

deaths_df = pd.read_csv(deaths_file)

# Being a little bit presumptuous that the column names don't change before I lose interest
state_lbl = 'Province/State'
country_lbl = 'Country/Region'

if __name__ == '__main__':
    country_list = ['US', 'Italy', 'China', 'Spain', 'Iran', 'France', 'Japan']
    death_lbl = ['Total Deaths', 'Daily Deaths']
    death_count_dict = {}
    plt.figure()
    for country in country_list:
        a = deaths_df.loc[
                (deaths_df[country_lbl] == country)
            ].iloc[:, 4:].sum(axis=0)
        a.index = pd.to_datetime(a.index)
        z = a.loc[a > 0].to_numpy()
        plt.plot(z)
        b = a[1:] - a[:-1].to_numpy()
        b = b.rolling(3).mean()
        death_count_dict[country + ' - ' + death_lbl[0]] = a
        death_count_dict[country + ' - ' + death_lbl[1]] = b
    plt.legend(country_list)
    plt.title('Total Deaths per Country by Days from First Confirmed Case')
    death_counts_df = pd.DataFrame(death_count_dict)
    death_counts_df.loc[:, death_counts_df.columns.str.contains('Daily')].plot(title='Deaths Per Day By Country')
    death_counts_df.loc[:, death_counts_df.columns.str.contains('Total')].plot(title='Total Deaths per Country by Date')